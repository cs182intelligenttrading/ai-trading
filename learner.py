import data_read
import numpy as np

class Model():
    def __init__(self, ticker, featurelist):
        self.dr = data_read.get_data(ticker)
        self.features = np.array(self.dr[featurelist])
        self.values = self.dr['close']
        self.gains = self.dr['gain']
        self.date = self.dr['date']
        self.weights = [0.1 for _ in range(len(self.features[0]))]
        self.alpha = 10e-6
        self.true_values = self.dr['should_buy']

    def findValue(self, day):
        value = 0
        features = self.features[day]
        for i in xrange(len(self.weights)):
            value += self.weights[i] * features[i]
        return value

    def findAction(self, day):
        value = self.findValue(day)
        self.update(self.true_values[day], value, self.features[day])
        if value > 0:
            return 'B'
        else:
            return 'S'

    def update(self, true_value, prediction, features):
        error = true_value - prediction
        for i in xrange(len(self.weights)):
            self.weights += error*self.alpha*features[i]

    def train(self, training_days, episodes = 5000):
        for episode in xrange(episodes):
            for day in xrange(training_days):
                prediction = self.findValue(day)
                self.update(self.true_values[day], prediction, self.features[day])
        self.alpha = 10e-6