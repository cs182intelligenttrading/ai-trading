import pandas as pd
import data_write

def get_frame(ticker):
    path = 'data_output/df/' + ticker + '_NNpredictions.csv'
    df = pd.read_csv(path)
    return df


def simulate(ticker):
    # initialize stock
    stockdf = get_frame(ticker)
    #print stockdf.head()
    age = 0
    portfolio, referenceportfolio = 10000, 10000
    hits = []
    ai_bull = []
    mkt_bull = []
    ai_bull_and_correct = []

    # initialize writer object
    writer = data_write.Writer(ticker)


    # simulate
    while age < len(stockdf)-1:
        belief = stockdf.iloc[age]['predictions']
        actual = stockdf.iloc[age]['gain_tomorrow']

        # update ai-portfolio
        if belief > -1:
            portfolio = portfolio + portfolio*actual/100

        # update bh-portfolio
        referenceportfolio = referenceportfolio + referenceportfolio*actual/100

        if belief*actual > 0: hits.append(1)
        else: hits.append(0)

        if actual > 0: mkt_bull.append(1)
        else: mkt_bull.append(0)

        if belief > 0:
            ai_bull.append(1)
            if actual > 0:
                ai_bull_and_correct.append(1)
            else:
                ai_bull_and_correct.append(0)
        else:
            ai_bull.append(0)

        # outputs some info on performance
        if not age % 365:
            print portfolio, '   ', referenceportfolio, '   ', portfolio - referenceportfolio

        # store day for output in writer-object
        writer.store({'ai': portfolio, 'bh': referenceportfolio, 'date': stockdf.iloc[age]['date']})

        # increment age by 1 day
        age += 1

    # make writer-object output to csv

    hitrate = round(float(sum(hits)) / len(hits), 4)
    print 'hitrate:', hitrate
    globalhit.append(hitrate)

    mkt_bull_share = round(float(sum(mkt_bull)) / len(mkt_bull), 4)
    print 'mkt_bull_share', mkt_bull_share

    ai_bull_share = round(float(sum(ai_bull)) / len(ai_bull), 4)
    print 'ai_bull_share', ai_bull_share

    ai_bull_n_correct_share = round(float(sum(ai_bull_and_correct)) / len(ai_bull_and_correct), 4)
    print 'ai_bull_and_correct_share', ai_bull_n_correct_share

    print "\n Writing results from", ticker, "to CSV output..."
    writer.write_to_csv()


def run(stocks):
    for ticker in stocks:
        print "\n Simulating", ticker, "..."
        simulate(ticker)

    print "\n Running R-script to visualize results..."
    print  'global hitrate:', sum(globalhit) / len(globalhit)
    data_write.visualize_to_png(stocks)
    print 'terminate'




globalhit = []
stocks = ['FORD']
run(stocks)