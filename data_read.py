import pandas as pd
import numpy as np
from talib.abstract import *
import pandas_datareader as web
import datetime

def get_data(ticker, startdate=datetime.datetime(2000, 1, 1), enddate =datetime.date.today()):
    df = web.DataReader(ticker, 'yahoo', startdate, enddate)
    df.index.names = ['date']
    df.columns = ['open', 'high', 'low', 'close', 'adj_close', 'volume']
    df['date'] = df.index
    return get_processed_df(df)

def get_df_from_file(filepath):
    raw_data = pd.read_csv(filepath)
    return raw_data

def preview_df(dataframe):
    print dataframe[400:420]
    print dataframe.index
    print dataframe.columns

def get_processed_df(df):
    # Adding daily gain
    df['prev'] = df['close'].shift(1)
    df['gain'] = (df['close'] - df['prev']) * 100 / df['prev']
    df['gain_tomorrow'] = df['gain'].shift(-1)
    df['should_buy'] = np.where(df['gain_tomorrow'] >= 0, [1], [0])

    # Adding SMAs
    df['sma_25'] = SMA(df, timeperiod=25)
    df['sma_50'] = SMA(df, timeperiod=50)
    df['sma_200'] = SMA(df, timeperiod=200)

    df['sma25_over_sma50'] = np.sign(df['sma_25']-df['sma_50'])

    # Adding EMAs
    df['ema_25'] = SMA(df, timeperiod=25)
    df['ema_50'] = SMA(df, timeperiod=50)
    df['ema_200'] = SMA(df, timeperiod=200)
    df['ema_25_fraction'] = df['ema_25']/df['close']
    df['ema_50_fraction'] = df['ema_50']/df['close']
    df['ema_200_fraction'] = df['ema_200']/df['close']

    # Adding day-diff
    df['daydiff'] = df['open']/df['close']

    # Adding MACDs
    # Positive MACD indicates that the 12-day EMA is above the 26-day EMA. Upside momentum is increasing.
    macd = MACD(df, fastperiod=12, slowperiod=26, signalperiod=9)
    df['macd'], df['macdsignal'], df['macdhist'] = macd['macd'], macd['macdsignal'], macd['macdhist']

    # MACD histogram. Difference between MACD and 9-day EMA. Should indicate positive
    df['macd_diff'] = df['macd'] - df['macdsignal']
    df['macd_prod'] = df['macd']*df['macdsignal']

    # Binary indicator of the macd_hist
    df['bin_macdhist'] = np.sign(df['macdhist'])
    df['bin_macd_centercross'] = np.sign(df['macd'])

    def f(inp):
        if inp['rsi_14'] > 60:
            val = 1
        elif inp['rsi_14'] > 40:
            val = -1
        else:
            val = -1
        return val

    # Adding RSI 12 and 100 + scaling down
    df['rsi_100'] = RSI(df, timeperiod=100)
    df['rsi_100'] = df['rsi_100']/100
    df['rsi_12'] = RSI(df, timeperiod=12)
    df['rsi_14'] = RSI(df, timeperiod=14)
    df['rsi_disc'] = df.apply(f, axis=1)

    # Adding OBV
    df['volume'] = df['volume'].astype(float)
    df['obv'] = OBV(df)

    df['ad'] = AD(df)
    df['adosc'] = ADOSC(df)
    # Adding HT Trendline
    df['ht_trendline'] = HT_TRENDLINE(df)

    # Adding Hammer
    df['hammer'] = CDLHAMMER(df)
    df['adx'] = ADX(df)
    df['adxr'] = ADXR(df)
    df['apo'] = APO(df)
    df['beta'] = BETA(df)
    df['mom'] = MOM(df)
    df['ppo'] = PPO(df)
    df['sar'] = SAR(df)
    df['sma'] = SMA(df)
    df['stochrsi'] = WILLR(df)
    df['ultosc'] = ULTOSC(df)
    df['const'] = 1

    # Remove rows with non-numeric values
    df = df.dropna()
    return df
