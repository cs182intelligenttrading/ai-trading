import numpy as np
import data_read

class Model():
    def __init__(self, ticker, alpha, featurelist):

        self.alpha = alpha # learning rate
        self.featurelist = featurelist
        self.dr = data_read.get_data(ticker)
        self.X = self.dr[featurelist]
        self.X = np.array(self.X) # 2d-matrix. Each row is on form f1, ..., fn representing features that day
        self.date = self.dr['date']
        self.prices = self.dr['gain'] # relative gain
        self.values = self.dr['close'] # close price
        self.gains = self.dr['gain']
        self.date = self.dr['date']
        self.hits = 0.0
        self.estimations = 0.0

        self.weights = [0.5 for _ in xrange(len(featurelist))]
        self.weights = np.array(self.weights)


    def getValue(self, observation):
        value = 0
        features = self.X[observation]
        for feature in xrange(len(features)):
            value += features[feature] * self.weights[feature]
        return value

    def updateWeights(self, prediction, true_value, features):
        error = true_value - prediction
        for l in xrange(len(self.weights)):
            self.weights[l] += self.alpha*error*features[l]

    def train(self, days_training, episodes, start=0):
        """
        Train the model over a specified number of time periods and episodes.
        Decay the learning rate once training is complete.
        """
        self.alpha = 10e-6
        for j in xrange(episodes):
            if j % 1000 == 0:
                print 'Episode ', j, ' out of ', episodes
                print 'Current weights: ', self.weights
                print '======'
            for d in xrange(start, days_training):
                prediction = self.getValue(d)
                truth = self.values[d+1]
                self.updateWeights(prediction, truth, self.X[d])
        self.alpha = 10e-7


    def findAction(self, day):
        self.estimations += 1
        prediction = self.getValue(day)
        if prediction > 0:
            return 'B'
        else:
            return 'S'

# Do we need this?
class Model_s():
    def __init__(self, ticker, alpha, featurelist):

        self.alpha = alpha  # learning rate
        self.featurelist = featurelist
        self.dr = data_read.get_data(ticker)
        self.X = self.dr[featurelist]
        self.X = np.array(self.X)  # 2d-matrix. Each row is on form f1, ..., fn representing features that day

        self.values = self.dr['close']  # close price
        self.gains = self.dr['gain']
        self.date = self.dr['date']
        self.hits = 0.0
        self.estimations = 0.0

        # initalize weights
        self.weights = []
        for k in xrange(len(featurelist)):
            self.weights.append(1.0)
        self.weights = np.array(self.weights)

    def getValue(self, observation):
        value = 0
        features = self.X[observation]
        for feature in xrange(len(features)):
            value += features[feature] * self.weights[feature]
        return value

    def updateWeights(self, prediction, true_value, features):
        error = true_value - prediction
        for l in xrange(len(self.weights)):
            self.weights[l] += self.alpha * error * features[l]

    def train(self, days_training, episodes, start=0):
        """
        Train the model over a specified number of time periods and episodes.
        Decay the learning rate once training is complete.
        """
        self.alpha = 10e-6
        for j in xrange(episodes):
            if j % 1000 == 0:
                print 'Episode ', j, ' out of ', episodes
                print 'Current weights: ', self.weights
                print '======'
            for d in xrange(start, days_training):
                prediction = self.getValue(d)
                truth = self.values[d + 1]
                self.updateWeights(prediction, truth, self.X[d])
        self.alpha = 10e-7

    def findAction(self, day):
        self.estimations += 1
        prediction = self.getValue(day)
        self.updateWeights(prediction, self.values[day + 1], self.X[day])
        # print 'We predict: ', prediction
        # print 'Turns out to be: ', self.values[day + 1]
        if prediction > self.values[day] and self.values[day + 1] > self.values[day]:
            self.hits += 1
        if prediction < self.values[day] and self.values[day + 1] < self.values[day]:
            self.hits += 1

        if self.estimations % 100 == 0:
            print 'Hit rate: ', str(self.hits / self.estimations)
        if prediction > self.values[day]:
            return 'B'
        else:
            return 'S'
