import collections, copy, random, pandas, stock, data_read as dr
from datetime import datetime, timedelta
import matplotlib.pyplot as plt


class ApproximateQ:
    """
    The approximate Q-learning agent is used to eliminate the need for a vast state space encountered in
    vanilla Q-learning.
    """

    def __init__(self, ticker, learning_rate, featurelist):
        self.learning_rate = learning_rate
        self.actions = ['B', 'S', 'NA'] # buy sell no action
        self.state = stock.Stock(ticker, featurelist)
        self.weights = [0.0 for _ in range(len(self.state.getFeatures()))]
        self.owned = None
        self.p = False #boolean to print debug messages.
        self.oldState = None

    # Legal actions is all actions adjusted for current holding
    def getLegalActions(self):
        legal_actions = copy.deepcopy(self.actions)
        owned = self.owned == 'H' or self.owned == 'B'
        if owned:
            legal_actions.remove('B')
        else:
            legal_actions.remove('S')
        return legal_actions


    def getQV(self):
        """
        Q-value from features
        """
        weights = self.weights
        features = self.state.getFeatures()
        qValue = 0
        for i in range(len(features)):
            qValue += weights[i] * features[i]
        return qValue


    def update(self, reward, value): # TODO: Denne kan umulig vaere riktig.
        error = value - reward
        #print 'value, reward, error:', value, reward, error
        #print 'weights:', self.weights
        #self.errors.append(error**2)
        if self.p:
            print "Error: ", error
        old_feat = self.oldState.getFeatures()
        for i in range(len(self.weights)):
            self.weights[i] = self.weights[i] - self.learning_rate*error*old_feat[i]
        if self.p:
            print "New weights: ", self.weights


    def findAction(self):
        """
        Find the best action. Use epsilon-greedy to explore more.
        """
        legal_actions = self.getLegalActions()
        qvalue = self.getQV()
        if self.p:
            print "Estimating value of state to:", qvalue

        scepticism = 0.15
        if qvalue >= scepticism and 'B' in legal_actions:
            if self.p:
                print "Buying"
            return 'B', qvalue
        elif qvalue >= scepticism:
            if self.p:
                print 'Holding'
            return 'H', qvalue
        else:
            if self.p:
                print 'Selling'
            return 'S', qvalue

    def doAction(self, action):
        #save old state
        self.oldState = copy.deepcopy(self.state)

        #transition and get reward
        reward = self.state.transition()

        #update ownership status
        if self.owned is None or self.owned is not 'H':
            self.owned = action

        return reward


    def doTrade(self, portfolio, action, reward, ownership):
        if not ownership:
            return portfolio
        if ownership == 'B' or ownership == 'H' and not action == 'S':
            return portfolio + portfolio*reward/100
        else:
            return portfolio


class ApproximateQ:
    """
    The approximate Q-learning agent is used to eliminate the need for a vast state space encountered in
    vanilla Q-learning.
    """

    def __init__(self, ticker, learning_rate, featurelist):
        self.learning_rate = learning_rate
        self.actions = ['B', 'S', 'NA']  # buy sell no action
        self.state = stock.Stock(ticker, featurelist)
        self.weights = [0.0 for _ in range(len(self.state.getFeatures()))]
        self.owned = None
        self.p = False  # boolean to print debug messages.
        self.oldState = None

    # Legal actions is all actions adjusted for current holding
    def getLegalActions(self):
        legal_actions = copy.deepcopy(self.actions)
        owned = self.owned == 'H' or self.owned == 'B'
        if owned:
            legal_actions.remove('B')
        else:
            legal_actions.remove('S')
        return legal_actions

    def getQV(self):
        """
        Q-value from features
        """
        weights = self.weights
        features = self.state.getFeatures()
        qValue = 0
        for i in range(len(features)):
            qValue += weights[i] * features[i]
        return qValue

    def update(self, reward, value):  # TODO: Denne kan umulig vaere riktig.
        error = value - reward
        # print 'value, reward, error:', value, reward, error
        # print 'weights:', self.weights
        # self.errors.append(error**2)
        if self.p:
            print "Error: ", error
        old_feat = self.oldState.getFeatures()
        for i in range(len(self.weights)):
            self.weights[i] = self.weights[i] - self.learning_rate * error * old_feat[i]
        if self.p:
            print "New weights: ", self.weights

    def findAction(self):
        """
        Find the best action. Use epsilon-greedy to explore more.
        """
        legal_actions = self.getLegalActions()
        qvalue = self.getQV()
        if self.p:
            print "Estimating value of state to:", qvalue

        scepticism = 0.15
        if qvalue >= scepticism and 'B' in legal_actions:
            if self.p:
                print "Buying"
            return 'B', qvalue
        elif qvalue >= scepticism:
            if self.p:
                print 'Holding'
            return 'H', qvalue
        else:
            if self.p:
                print 'Selling'
            return 'S', qvalue

    def doAction(self, action):
        # save old state
        self.oldState = copy.deepcopy(self.state)

        # transition and get reward
        reward = self.state.transition()

        # update ownership status
        if self.owned is None or self.owned is not 'H':
            self.owned = action

        return reward

    def doTrade(self, portfolio, action, reward, ownership):
        if not ownership:
            return portfolio
        if ownership == 'B' or ownership == 'H' and not action == 'S':
            return portfolio + portfolio * reward / 100
        else:
            return portfolio
