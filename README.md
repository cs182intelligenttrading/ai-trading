This is the delivered program for the course project in CS182 Artificial Intelligence at Harvard University.

Authors:
Sondre Jensen
Peder Gjerstad

External dependencies:
* Graphing
- R
- R library: 'readr'
- R library: 'ggplot2'
- R library: 'tidyr'

* Main scripts
- python 2.7.x
- python library: 'pandas'
- python library: 'talib.abstract'
- python library: 'pandas_datareader'
- python library: 'datetime'