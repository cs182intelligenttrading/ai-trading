import ql
import copy
import data_write, data_read
import qlearning
import matplotlib.pyplot as plt
import random
import settings
import statistics

# Learns on trainingperiod. Then does online leastsquares on the rest.
def simulate_approx_q(ticker, featurelist, trainingperiod):
    # initialize stock
    learning_rate = 0.0002
    agent = ql.ApproximateQ(ticker, learning_rate, featurelist)

    age = 0
    portfolio, referenceportfolio = 10000, 10000

    # initialize writer object and stats object
    writer = data_write.Writer(ticker)
    stats = statistics.Statistics()

    # simulate
    while agent.state.isNotDoomsday():
        ownership = copy.deepcopy(agent.owned)
        nextaction, value = agent.findAction()
        reward = agent.doAction(nextaction)
        # training complete, begin trade
        if age >= trainingperiod:
            portfolio = agent.doTrade(portfolio, nextaction, reward, ownership)
            referenceportfolio = agent.doTrade(referenceportfolio, 'B', reward, 'B')

            stats.register(
                hit = value*reward > 0,
                ai_bull = value > 0,
                mkt_bull = reward > 0,
            )

        # update weights according to reward
        agent.update(reward, value)

        # outputs some info on performance
        if not age % 365:
            print portfolio, '   ', referenceportfolio, '   ', portfolio - referenceportfolio
            agent.learning_rate *= 1

        # store day for output in writer-object
        writer.store({'ai': portfolio,
                      'bh': referenceportfolio,
                      'date': agent.state.date,
                      'long': 0})

        # increment age by 1 day
        age += 1

    # make writer-object output to csv
    print '\n final weights:', agent.weights
    stats.printStats()
    print "\n Writing results from", ticker, "to CSV output..."
    writer.write_to_csv()

# Uses set rules. Works
def simulate_nonlearner(ticker, features, trainingperiod):
    # initialize stock
    stockdf = data_read.get_data(ticker)
    age = 0
    portfolio, referenceportfolio = 10000, 10000

    # initialize writer object
    writer = data_write.Writer(ticker)
    stats = statistics.Statistics()

    # simulate
    while age < len(stockdf) - 1:
        belief = stockdf.iloc[age]['rsi_14']
        actual = stockdf.iloc[age]['gain_tomorrow']

        decide_to_buy = belief > 0
        # update ai-portfolio
        if decide_to_buy:
            portfolio = portfolio + portfolio * actual / 100

        # update bh-portfolio
        referenceportfolio = referenceportfolio + referenceportfolio * actual / 100

        stats.register(
            hit = decide_to_buy == stockdf.iloc[age]['should_buy'],
            ai_bull = decide_to_buy > 0,
            mkt_bull = stockdf.iloc[age]['should_buy']
        )

        # outputs some info on performance
        if not age % 365:
            print portfolio, '   ', referenceportfolio, '   ', portfolio - referenceportfolio

        # store day for output in writer-object
        writer.store({'ai': portfolio, 'bh': referenceportfolio, 'date': stockdf.iloc[age]['date'], 'long':
            max(belief*1000, 0)})

        # increment age by 1 day
        age += 1

    # make writer-object output to csv

    globalhit.append(stats.getHitRate())
    stats.printStats()
    print 'portfolio:', portfolio
    print 'buy_hold:', referenceportfolio
    print "\n Writing results from", ticker, "to CSV output..."
    writer.write_to_csv()
    return stats.getHitRate()

# Trades randomly. Works.
def simulate_random(ticker, features, trainingperiod):
    # initialize stock
    stockdf = data_read.get_data(ticker)
    age = 0
    portfolio, referenceportfolio = 10000, 10000

    # initialize writer object
    writer = data_write.Writer(ticker)
    stats = statistics.Statistics()

    # simulate
    for age in range(len(stockdf) - 1):
        actual = stockdf.iloc[age]['gain_tomorrow']

        decide_to_buy = random.random() > 0.5
        # update ai-portfolio
        if decide_to_buy:
            portfolio = portfolio + portfolio * actual / 100
            buy_int = 1
        else:
            buy_int = 0

        # update bh-portfolio
        referenceportfolio = referenceportfolio + referenceportfolio * actual / 100

        stats.register(
            hit=decide_to_buy == stockdf.iloc[age]['should_buy'],
            ai_bull=decide_to_buy > 0,
            mkt_bull=stockdf.iloc[age]['should_buy']
        )

        # outputs some info on performance
        if not age % 365:
            print portfolio, '   ', referenceportfolio, '   ', portfolio - referenceportfolio

        # store day for output in writer-object
        writer.store({'ai': portfolio,
                      'bh': referenceportfolio,
                      'date': stockdf.iloc[age]['date'],
                      'long':buy_int*5000})

    globalhit.append(stats.getHitRate())
    stats.printStats()
    print 'portfolio:', portfolio
    print 'buy_hold:', referenceportfolio
    print "\n Writing results from", ticker, "to CSV output..."
    writer.write_to_csv()
    return stats.getHitRate()

# Trains every year. Uses absolute stock prices. Works.
def simulate_absolute_offline(ticker, featurelist, trainingperiod):
    model = qlearning.Model(ticker, 10e-4, featurelist)
    model.train(days_training=trainingperiod, episodes=1000)
    portfolio = 10000
    bh_portfolio = 10000

    has_stock = False
    writer = data_write.Writer(ticker)
    stats = statistics.Statistics()

    for i in xrange(trainingperiod + 1, len(model.values) - 1):
        if i % 365 == 0:
            print 'I: ', i
            print 'Recalibrate...'
            model.train(days_training=i, episodes=1000, start=i - 365)
        best_action = model.findAction(i)

        stats.register(
            hit=(model.gains[i + 1] > 0) == (best_action == 'B'),
            ai_bull=best_action == 'B',
            mkt_bull=model.gains[i+1] > 0,
        )

        if has_stock and best_action == 'S':
            has_stock = False
        if not has_stock and best_action == "B":
            has_stock = True

        # Update own portfolio if owned
        if has_stock:
            portfolio = portfolio + portfolio * model.gains[i + 1] / 100
            has_stock_int = 1
        else:
            has_stock_int = 0

        # Update buy-hold portfolio
        bh_portfolio = bh_portfolio + bh_portfolio * model.gains[i + 1] / 100

        writer.store({'ai': portfolio, 'bh': bh_portfolio, 'date': model.date[i], 'long': has_stock_int*5000})
    writer.write_to_csv()
    stats.printStats()
    print 'portfolio:', portfolio
    print 'buy_hold:', bh_portfolio
    return stats.getHitRate()

# Learn first, then trade. No learning online. Works
def simulate_relative_offline(ticker, featurelist, trainingperiod):
    import learner
    model = learner.Model(ticker, featurelist)
    model.train(trainingperiod)

    portfolio, bh_portfolio, has_stock = 10000, 10000, False
    writer = data_write.Writer(ticker)
    stats = statistics.Statistics()

    for i in xrange(1, len(model.values) - 1):
        best_action = model.findAction(i)

        stats.register(
            hit = (model.gains[i + 1] > 0) == (best_action == 'B'),
            ai_bull = best_action == 'B',
            mkt_bull = model.gains[i + 1] > 0
        )

        # Decide if position should be changed
        if has_stock and best_action == 'S': has_stock = False
        if not has_stock and best_action == "B": has_stock = True

        # Update own portfolio value if owned
        if has_stock:
            portfolio = portfolio + portfolio * model.gains[i + 1] / 100

        # Update buy-hold portfolio
        bh_portfolio = bh_portfolio + bh_portfolio * model.gains[i + 1] / 100


        if has_stock: has_stock_int = 1
        else: has_stock_int = 0

        writer.store({'ai': portfolio, 'bh': bh_portfolio, 'date': model.date[i], 'long': has_stock_int * 5000})

    writer.write_to_csv()
    stats.printStats()
    print 'portfolio:', portfolio
    print 'buy_hold:', bh_portfolio
    return stats.getHitRate()

def select_function():
    print 'Available functions:'
    print s.getAvailableFunctions()
    function = int(input('Choose one of the above functions (number):'))
    s.setFunction(function)

def run(stocks, featurelist, trainingperiod=365):
    for ticker in stocks:
        print "\n Simulating", ticker, 'with', s.functions[s.selected_function], "..."
        if s.selected_function == 1:
            globalhit.append(simulate_approx_q(ticker, featurelist, trainingperiod))
        if s.selected_function == 2:
            globalhit.append(simulate_nonlearner(ticker, featurelist, trainingperiod))
        if s.selected_function == 3:
            globalhit.append(simulate_random(ticker, featurelist, trainingperiod))
        if s.selected_function == 4:
            globalhit.append(simulate_absolute_offline(ticker, featurelist, trainingperiod))
        if s.selected_function == 5:
            globalhit.append(simulate_relative_offline(ticker, featurelist, trainingperiod))

    if s.make_graph:
        print "\n Running R-script to visualize results..."
        data_write.visualize_to_png(stocks)
    if len(globalhit) > 0 and all(globalhit):
        print globalhit
        print  'terminating with global hitrate:', sum(globalhit)/len(globalhit)


globalhit = []
s = settings.Settings()
select_function()
run(s.getStocs(), s.getFeatures())