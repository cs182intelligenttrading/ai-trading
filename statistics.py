class Statistics:
    def __init__(self):
        self.ai_bull = []
        self.mkt_bull = []
        self.ai_bull_and_correct = []
        self.hits = []

    def register(self, hit, ai_bull, mkt_bull):
        if hit: self.hits.append(1)
        else: self.hits.append(0)

        if ai_bull: self.ai_bull.append(1)
        else: self.ai_bull.append(0)

        if mkt_bull: self.mkt_bull.append(1)
        else: self.mkt_bull.append(0)

        if ai_bull and mkt_bull: self.ai_bull_and_correct.append(1)
        else: self.ai_bull_and_correct.append(0)

    def getHitRate(self):
        if len(self.hits) == 0:
            return 'NA'
        return float(sum(self.hits))/len(self.hits)

    def getAIBullRate(self):
        if len(self.ai_bull) == 0:
            return 'NA'
        return float(sum(self.ai_bull))/len(self.ai_bull)

    def getMktBullRate(self):
        if len(self.mkt_bull) == 0:
            return 'NA'
        return float(sum(self.mkt_bull))/len(self.mkt_bull)

    def getAIBullNC(self):
        if len(self.ai_bull_and_correct) == 0:
            return 'NA'
        return float(sum(self.mkt_bull))/len(self.mkt_bull)

    def printStats(self):
        print 'hitrate:', self.getHitRate()
        print 'mkt_bull_rate:', self.getMktBullRate()
        print 'ai_bull_rate', self.getAIBullRate()
        print 'ai_bull_and_correct_rate', self.getAIBullNC()