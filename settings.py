class Settings():
    def __init__(self):
        self.features = {
            # Simple Moving Averages. Number indicates number of days.
            'sma_25': False,
            'sma_50': False,
            'sma_200': False,

            # Exponential Moving Averages. Number indicates number of days.
            'ema_25': False,
            'ema_50': False,
            'ema_200': False,

            # EMA divided by close price.
            'ema_25_fraction': False,
            'ema_50_fraction': False,
            'ema_200_fraction': False,

            # Opening price divided by closing price
            'daydiff': False,

            # Admin indicators
            # - what would have been the correct decision
            'should_buy': False,
            # - previous day's close price
            'prev': False,
            # - the gain from yesterday to today
            'gain': False,
            # - the gain from today to tomorrow
            'gain_tomorrow': False,
            # - constant for use in approx q-learning
            'const': True, # IMPORTANT! Must be true for linear approx

            # Relative strength indexes. Number indicates #days
            'rsi_12': False,
            'rsi_14': False,
            'rsi_100': False,

            # Moving Average Covergence Divergence
            # Positive MACD indicates that the 12-day EMA is above the 26-day EMA. Upside momentum is increasing.
            'macd': False,
            'macd_signal': False,
            'macdhist': True,

            # Discrete indicators
            # - rsi_12. 1 if > 70, -1 if < 30, 0 else
            'rsi_disc': True,
            # - 1 if sma_25 is over sma_50, -1 else
            'sma25_over_sma50': True,
            'bin_macdhist': False,
            'bin_macd_centercross': False,

            #OBV
            'obv': False,

            'ad': False,
            'adosc': False,
            'ht_trendline': False,

            # Other
            'hammer': False,
            'adx': False,
            'adxr': False,
            'apo': False,
            'beta': False,
            'mom': False,
            'ppo': False,
            'sar': False,
            'sma': False,
            'stochrsi': False,
            'ultosc': False
        }
        self.stocks = {'AAPL': False,
                       'AMZN': False,
                       'BRK-B': False,
                       'FB': True,
                       'GOOGL': False,
                       'JNJ': False,
                       'JPM': False,
                       'MSFT': False,
                       'XOM': False,
                       'MMM': False,
                       'AXP': False,
                       'BA': False,
                       'CAT': False,
                       'CVX': False,
                       'CSCO': False,
                       'KO': False,
                       'INTC': False,
                       'FORD': False,
                       'F': False,
                       'PG': False
                       }
        self.functions = {
            1: 'simulate_approx_q',
            2: 'simulate_nonlearner',
            3: 'simulate_random',
            4: 'simulate_absolute_offline',
            5: 'simulate_relative_offline'
        }
        self.selected_function = 5
        '''
        If this is set to true, the working directory
        in the R file and this python script has to be the same!
        This need to be edited manually in the R-script.
        '''
        self.make_graph = False


    def getFeatures(self):
        to_return = []
        for ind in self.features:
            if self.features[ind]:
                to_return.append(ind)
        return to_return

    def getStockIndex(self, index):
        indexes = {'snp500': ['AAPL', 'AMZN', 'BRK-B', 'FB', 'GOOGL', 'JNJ', 'JPM', 'MSFT', 'XOM'],
                   'dow': ['MMM', 'AXP', 'AAPL', 'BA', 'CAT', 'CVX', 'CSCO', 'KO']}
        if index in indexes:
            return indexes[index]

    def getAvailableStocs(self):
        return self.stocks.keys()

    def getStocs(self):
        to_return = []
        for ind in self.stocks:
            if self.stocks[ind]:
                to_return.append(ind)
        return to_return

    def getAvailableFeatures(self):
        return self.features.keys()

    def getAvailableFunctions(self):
        out = ''
        for f in self.functions:
            out += self.functions[f] +': ' + str(f) + '\n'
        return out

    def setFunction(self, func):
        self.selected_function = func