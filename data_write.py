import pandas
import subprocess

def write_to_csv(dict, ticker):
    df = pandas.DataFrame(dict)
    path = 'data_output/' + ticker + '_output.cvs'
    outputfile = open(name=path, mode='w')
    outputfile.write(df.to_csv())
    outputfile.close()


def visualize_to_png(tickers):
    # Run R-script to make graphs
    command = 'Rscript'
    script_path = 'trade_visualizer.R'
    args = tickers
    subprocess.check_output([command, script_path] + args)


def store_df(df, ticker):
    path = 'data_output/df/' + ticker + '_df.csv'
    outputfile = open(name=path, mode='w')
    outputfile.write(df.to_csv())
    outputfile.close()

class Writer:

    def __init__(self, ticker):
        self.dict = {'ai': [], 'bh': [], 'diff': [], 'date': [], 'long': []}
        self.ticker = ticker

    def store(self, day):
        self.dict['ai'].append(day['ai'])
        self.dict['bh'].append(day['bh'])
        self.dict['long'].append(day['ai'] - day['bh'])
        self.dict['date'].append(day['date'])
        self.dict['diff'].append(day['long'])
        #if 'long' in day.keys():
        #    self.dict['long'].append(day['long'])


    def write_to_csv(self):
        df = pandas.DataFrame(self.dict)
        path = 'data_output/' + self.ticker + '_output.cvs'
        outputfile = open(name=path, mode='w')
        outputfile.write(df.to_csv())
        outputfile.close()