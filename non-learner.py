import ql
import copy
import data_write, data_read
import qlearning
import matplotlib.pyplot as plt
import random
import pandas as pd

def simulate_new(ticker):
    # initialize stock
    stockdf = data_read.get_data(ticker)
    # print stockdf.head()
    age = 0
    portfolio, referenceportfolio = 10000, 10000
    hits = []
    ai_bull = []
    mkt_bull = []
    ai_bull_and_correct = []

    # initialize writer object
    writer = data_write.Writer(ticker)

    # simulate
    while age < len(stockdf) - 1:
        belief = stockdf.iloc[age]['sma25_over_sma50']
        belief += stockdf.iloc[age]['bin_macdhist']
        belief += stockdf.iloc[age]['rsi_disc']
        if belief > 1:
            belief = 1
        else:
            belief = 0
        actual = stockdf.iloc[age]['gain_tomorrow']

        # update ai-portfolio
        if belief > 0:
            portfolio = portfolio + portfolio * actual / 100

        # update bh-portfolio
        referenceportfolio = referenceportfolio + referenceportfolio * actual / 100

        if belief * actual > 0:
            hits.append(1)
        else:
            hits.append(0)

        if actual > 0:
            mkt_bull.append(1)
        else:
            mkt_bull.append(0)

        if belief > 0:
            ai_bull.append(1)
            if actual > 0:
                ai_bull_and_correct.append(1)
            else:
                ai_bull_and_correct.append(0)
        else:
            ai_bull.append(0)

        # outputs some info on performance
        if not age % 365:
            print portfolio, '   ', referenceportfolio, '   ', portfolio - referenceportfolio

        # store day for output in writer-object
        writer.store({'ai': portfolio, 'bh': referenceportfolio, 'date': stockdf.iloc[age]['date'], 'long':
            max(belief * 1000, 0)})

        # increment age by 1 day
        age += 1

    # make writer-object output to csv

    hitrate = round(float(sum(hits)) / len(hits), 4)
    print 'hitrate:', hitrate
    globalhit.append(hitrate)

    mkt_bull_share = round(float(sum(mkt_bull)) / len(mkt_bull), 4)
    print 'mkt_bull_share', mkt_bull_share

    ai_bull_share = round(float(sum(ai_bull)) / len(ai_bull), 4)
    print 'ai_bull_share', ai_bull_share

    # ai_bull_n_correct_share = round(float(sum(ai_bull_and_correct)) / len(ai_bull_and_correct), 4)
    # print 'ai_bull_and_correct_share', ai_bull_n_correct_share

    print "\n Writing results from", ticker, "to CSV output..."
    writer.write_to_csv()

    def run_new(stocks):
        for ticker in stocks:
            print "\n Simulating", ticker, "..."
            simulate(ticker)

        print "\n Running R-script to visualize results..."
        print  'global hitrate:', sum(globalhit) / len(globalhit)
        data_write.visualize_to_png(stocks)
        print 'terminate'


def run(stocks, featurelist):
    for ticker in stocks:
        print "\n Simulating", ticker, "..."
        simulate_new(ticker)  # , 365, featurelist)

    print "\n Running R-script to visualize results..."
    data_write.visualize_to_png(stocks)
    print  'terminating with global hitrate:', sum(globalhit) / len(globalhit)


globalhit = []
snp500 = ['AAPL', 'AMZN', 'BRK-B', 'FB', 'GOOGL', 'JNJ', 'JPM', 'MSFT', 'XOM']
dow = ['MMM', 'AXP', 'AAPL', 'BA', 'CAT', 'CVX', 'CSCO', 'KO']
alt = ['INTC', 'FORD', 'PG', 'BA']
alt = ['HD']
# featurelist = ['rsi_100', 'macd_diff', 'macd_prod']#, 'ema_25_fraction', 'ema_50_fraction', 'ema_200_fraction', 'daydiff']
featurelist = ['bin_macdhist']
# run(dow, featurelist)

run(alt, featurelist)