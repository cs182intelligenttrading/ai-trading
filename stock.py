import collections, copy, random, pandas, data_read as dr
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

class Stock:
    """
    The stock is represented by a vector containing the share price
    at the given day and all the features MA20, MA50, MA100.
    """
    def __init__(self, stockname, featurelist, start_day = 200):
        """
        Initialize at day 100 of data set. This is the earliest possible to be able to calculate data for all
        technical indicators.
        """
        self.row = start_day
        self.dataframe = dr.get_data(stockname)
        self.price = self.dataframe.iloc[start_day]['close']
        self.featurelist = featurelist
        self.features = [self.dataframe.iloc[start_day][a] for a in featurelist]
        self.date = self.dataframe.iloc[start_day]['date']
        self.gain = self.dataframe.iloc[start_day]['gain']

    def setDate(self, new_date):
        try:
            self.date = datetime(new_date)
        except:
            return 'e'

    def setPrice(self, new_price):
        self.price = new_price

    def setFeatures(self, new_features):
        self.features = new_features

    def getPrice(self):
        try:
            return self.price
        except:
            return 'e'

    def getFeatures(self):
        try:
            return self.features
        except:
            return 'e'

    def getData(self):
        """
        Returns a list of price and features of the current stock at current state.
        """
        try:
            return [self.date, self.price, self.features]
        except:
            return 'e'


    def transition(self, days_passed = 1):
        """
        The transitions are deterministic. Meaning that independent of action taken
        time advances to the next day resulting in a new share price and new array of vectors.
        We are always able to sell all or buy all that we want whatever market demand/supply is.

        Returns reward
        """
        self.row += days_passed
        self.price = self.dataframe.iloc[self.row]['close']
        self.features = [self.dataframe.iloc[self.row][f] for f in self.featurelist]
        self.date = self.dataframe.iloc[self.row]['date']
        self.gain = self.dataframe.iloc[self.row]['gain']
        return self.getReward()

    def getReward(self):
        return self.gain

    def isNotDoomsday(self):
        if self.row != len(self.dataframe.index)-1:
            return True
        else:
            return False